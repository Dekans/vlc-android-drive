package org.videolan.driveplugin;

import android.content.Intent;

import org.videolan.vlc.extensions.api.VLCExtensionService;

public class ExtensionService extends VLCExtensionService {

    DriveApplication mApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = (DriveApplication) getApplication();
        mApplication.setService(this);
    }


    @Override
    protected void browse(String stringId) {
        startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onInitialize() {
        browse(null);
    }

    @Override
    protected void refresh() {}
}
