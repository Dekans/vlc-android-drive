package org.videolan.driveplugin;

import android.accounts.Account;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.common.api.GoogleApiClient;

public class DriveApplication extends Application {

    public static final String EMAIL_KEY = "user_account";

    GoogleApiClient mGoogleApiClient;
    private  ExtensionService mService;
    private  String mToken;
    private  String mEmail;
    private Account mAccount;
    private  SharedPreferences mSettings;

    @Override
    public void onCreate() {
        super.onCreate();

        mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        mEmail = mSettings.getString(EMAIL_KEY, null);
    }

    public void setService(ExtensionService extensionService) {
        mService = extensionService;
    }

    public ExtensionService getService() {
        return mService;
    }

    public String getAccountName() {
        return mEmail;
    }

    public void setAccountName(String name) {
        mEmail = name;
        mSettings.edit().putString(DriveApplication.EMAIL_KEY, mEmail).apply();
    }

    public void setAccount(Account account){
        mAccount = account;
    }

    public Account getAccount() {
        return mAccount;
    }


    public String getToken(){
        return mToken;
    }

    public void setToken(String token){
        mToken = token;
    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void setGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        this.mGoogleApiClient = mGoogleApiClient;
    }
}
