package org.videolan.driveplugin;

import android.Manifest;
import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;

import org.videolan.vlc.extensions.api.tools.Dialogs;

import java.io.IOException;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DialogInterface.OnCancelListener {

    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;

    DriveApplication mApplication;
    private GoogleApiClient mGoogleApiClient;
    private String mToken;
    private String mEmail;
    private android.accounts.Account mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (DriveApplication) getApplication();
        mEmail = mApplication.getAccountName();
        mAccount = mApplication.getAccount();
        mToken = mApplication.getToken();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!TextUtils.isEmpty(mToken))
            connect();
        else if (!TextUtils.isEmpty(mEmail))
            checkPermission();
        else
            pickUserAccount();
    }

    private void connect() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .setAccountName(mEmail)
                .build();
        mGoogleApiClient.connect();
        mApplication.setGoogleApiClient(mGoogleApiClient);
    }

    private void checkPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getAccount();
            getAccessToken();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_GET_ACCOUNTS);
        }
    }

    private void getAccount() {
        mAccount = null;
        AccountManager accountManager = AccountManager.get(getApplication());
        android.accounts.Account[] accounts = accountManager.getAccountsByType("com.google");
        for (android.accounts.Account account : accounts) {
            if (account.name.equals(mEmail)) {
                mAccount = account;
                break;
            }
        }
        mApplication.setAccount(mAccount);
    }

    private static final int REQUEST_GET_ACCOUNTS = 1002;
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_GET_ACCOUNTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getAccount();
                    getAccessToken();
                } else {
                    mHandler.sendEmptyMessage(PERMISSION_NOT_RECEIVED);
                    finish();
                }
            }
        }
    }

    private void getAccessToken() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String scope = "oauth2:" + Scopes.DRIVE_FILE;
                    //mToken = GoogleAuthUtil.getToken(LoginActivity.this, mEmail, scope);
                    mToken = GoogleAuthUtil.getToken(LoginActivity.this, mAccount, scope);
                    mApplication.setToken(mToken);
                    connect();
                } catch (IOException e) {
                    Log.e("plugin", "IOException ", e);
                } catch (GooglePlayServicesAvailabilityException e) {
                    mHandler.obtainMessage(MSG_GPS_NOT_FOUND).sendToTarget();
                } catch (GoogleAuthException e) {
                    mHandler.obtainMessage(MSG_CONNECTION_ERROR).sendToTarget();
                    Log.e("plugin", "GoogleAuthException ", e);
                }
            }
        }).start();
    }

    @Override
    public void onConnected(Bundle bundle) {
        startFilePicker();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_AUTHORIZATION);
            } catch (IntentSender.SendIntentException e) {
            }
        } else
            mHandler.obtainMessage(MSG_CONNECTION_ERROR).sendToTarget();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    // Make sure the app is not already connected or attempting to connect
                    if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.connect();
                        Toast.makeText(this, R.string.connected, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                // Receiving a result from the AccountPicker
                if (resultCode == RESULT_OK) {
                    mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    mApplication.setAccountName(mEmail);
                    checkPermission();
                } else if (resultCode == RESULT_CANCELED) {
                    // The account picker dialog closed without selecting an account.
                    // Notify users that they must pick an account to proceed.
                    finish();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        try {
            startActivityForResult(intent, REQUEST_ACCOUNT_PICKER);
        } catch (ActivityNotFoundException e) {
            //TODO
        }
    }

    private void startFilePicker() {
        finish();
        Intent intent = new Intent(this, DriveActivity.class);
        intent.putExtra("action", DriveActivity.REQUEST_PICK_FILE);
        startActivity(intent);
    }

    private static final int MSG_CONNECTION_ERROR = 1000;
    private static final int MSG_GPS_NOT_FOUND = 1001;
    private static final int PERMISSION_NOT_RECEIVED = 1003;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CONNECTION_ERROR:
                    mApplication.setAccountName("");
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle(R.string.auth_error_title);
                    builder.setNegativeButton(R.string.exit_button_title, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            finish();
                        }
                    });
                    builder.setPositiveButton(R.string.select_email_button_title, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pickUserAccount();
                        }
                    });
                    builder.show();
                    //Dialogs.showAlertDialog(LoginActivity.this, R.string.auth_error_title, R.string.auth_error_message, LoginActivity.this);
                    break;
                case MSG_GPS_NOT_FOUND:
                    Dialogs.showAlertDialog(LoginActivity.this, R.string.gps_error_title, R.string.gps_error_message, LoginActivity.this);
                    break;
                case PERMISSION_NOT_RECEIVED:
                    Toast.makeText(LoginActivity.this, R.string.permission_not_received, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onCancel(DialogInterface dialog) {
        finish();
    }
}

