package org.videolan.driveplugin;

import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

public class DriveActivity extends AppCompatActivity{

    public static final int REQUEST_PICK_FILE = 1;
    private static final int SEND_URI = 2;

    private static DriveApplication sApplication;
    private GoogleApiClient mGoogleApiClient;
    private String mToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sApplication = (DriveApplication) getApplication();
        mGoogleApiClient = sApplication.getmGoogleApiClient();
        mToken = sApplication.getToken();

        startAction(getIntent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        startAction(intent);
    }

    private void startAction(Intent intent) {
        switch (intent.getIntExtra("action", 0)) {
            case REQUEST_PICK_FILE:
                startFilePicker();
                break;
        }
    }

    private void startFilePicker() {
        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
                .setMimeType(Utils.mimeTypes)
                .build(mGoogleApiClient);
        try {
            startIntentSenderForResult(
                    intentSender, REQUEST_PICK_FILE, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.i("plugin", "Unable to send intent", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    /*get the selected item's ID*/
                    final DriveId driveId = data.getParcelableExtra(
                            OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);//this extra contains the drive id of the selected file

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            com.google.api.services.drive.Drive drive = new com.google.api.services.drive.Drive.Builder(
                                    AndroidHttp.newCompatibleTransport(),
                                    JacksonFactory.getDefaultInstance(),
                                    new HttpRequestInitializer() {
                                        @Override
                                        public void initialize(HttpRequest request) throws IOException {
                                            request.getHeaders().setAuthorization("Bearer " + mToken);
                                        }
                                    })
                                    .build();
                            try {
                                com.google.api.services.drive.model.File file = drive.files().get(driveId.getResourceId()).execute();
                                String link = file.getDownloadUrl() + "&access_token=" + mToken;
                                sHandler.obtainMessage(SEND_URI,
                                        new DriveItem(link, file.getTitle())).sendToTarget();
                            } catch (GoogleJsonResponseException e) {
                                Log.e("plugin", "failed to retrieve picked file: ", e);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }
                finish();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static class DriveItem {
        Uri uri;
        String title;

        DriveItem(String uri, String title) {
            this.uri = Uri.parse(uri);
            this.title = title;
        }
    }

    private static Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SEND_URI:
                    ExtensionService service = sApplication.getService();
                    if (service == null)
                        return;
                    DriveItem item = (DriveItem) msg.obj;
                    service.playUri(item.uri, item.title);
            }
        }
    };
}
